# Dive Planning Slate

The dive planning slate is created as an introduction into the [Lit framework](https://lit.dev). To give some purpose to this project, the dive planner is intended to be a functional tool for calculating important numbers derived from a given dive plan. These numbers include the gas volume, gas reserve and expected CNS value. Keep in mind that this project is currently under construction and far away from being used for real dives.

## Install and Setup

Perform the following commands on your system in order to install the project:

- `git clone git@gitlab.com:ErikWe/dive-planning-slate.git`
- `cd dive-planning-slate`
- `npm install`

To make changes and get the project running locally, use the following commands:

- `npm run dev` - hosts the project on your system
- `npm run format` - commands Prettier to format your code
- `npm run check` - commands Lint to check for improvable code sections
- `npm run build` - builds the project into the _dist_ directory

## How-To

Currently this tool contains two sections, the upper section for the _dive plan_ and a lower section of the _gas overview_. The setup does not allow you to calculate a dive plan based on a decompression algorithm such as the _Bühlmann ZHL-16C_ but I will be able to calculate your required gases based on a given dive plan. To calculate a dive plan, please use professional software in order to create a save and reliable dive profile.

As soon as you have your dive plan with all the contingencies ready, you are provided with two options to enter them into this calculator:

1. Manually add each individual dive step by clicking on _Add DiveStep_
2. By importing a CSV file in the format _dept,duration,gas_ e.g. _6,10,N99_

After adding your dive plan, you already see derived information about your volume, PO2 as well as CNS values. To finish and finally calculate the required gas overview, click on the _Calculate_ button.
Now, the overview will display the total CNS exposure of your dive plan as well as the required amount of tanks to be carried with in order to safely return to the surface.

Keep in mind that this is a hobby project and thus **not** under any supervision regarding its correct functionality.

## Current Constraints

- Gases are limited to N21, N32, N99
- SAC of 20 Liter per minute
- Depth of 50 meter
- Duration for a single dive step of 20 minutes
- Tank size of 12 Liter
- Tank pressure of 200 Bar

## Planned features

- Customizable values for: gases (N07 - N99), depth, duration, SAC, tank size and tank pressure
- Warnings for values which are not conservative (e.g. PO2 > 1.4)
- Alerts for values which exceeds allowed limits (e.g. PO2 > 1.6)
- Streamlined UX (Less clicks required)
- Better graphic and design (proper CSS)

## Abandoned features

- Support for Trimix gases

## License

dive-planning-slate
Copyright (C) 2022 ErikWe

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
