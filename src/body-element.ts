import { LitElement, html } from "lit";
import { customElement } from "lit/decorators.js";
import "./table-element.ts";

@customElement("body-element")
export class BodyElement extends LitElement {
  render() {
    return html`
      <div>
        <center>
          <table-element></table-element>
        </center>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "body-element": BodyElement;
  }
}
