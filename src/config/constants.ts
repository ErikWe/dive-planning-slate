export const surfaceConstumptionRate = 20;

export const cns_min_map = new Map<number, number>();
cns_min_map.set(1.6, 2.22);
cns_min_map.set(1.5, 0.84);
cns_min_map.set(1.4, 0.65);
cns_min_map.set(1.3, 0.56);
cns_min_map.set(1.2, 0.48);
cns_min_map.set(1.1, 0.42);
cns_min_map.set(1.0, 0.34);
cns_min_map.set(0.9, 0.28);
cns_min_map.set(0.8, 0.22);
cns_min_map.set(0.7, 0.18);
cns_min_map.set(0.6, 0.14);

export const gas_map = new Map<string, number>();
gas_map.set("N21", 0.21);
gas_map.set("N32", 0.32);
gas_map.set("N99", 0.99);

export const depthList: number[] = Array.from({ length: 50 }, (_, i) => i + 1);
export const durationList: number[] = Array.from(
  { length: 20 },
  (_, i) => i + 1
);
export const gasList: string[] = ["N21", "N32", "N99"];

export const notAvailable = "N/A";
export const tank_size = 12;
export const tank_pressure = 200;
