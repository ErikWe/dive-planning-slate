import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement("footer-element")
export class FooterElement extends LitElement {
  @property()
  copyright = "© 2022 Erik Weinstock";

  render() {
    return html`
      <div>
        <center>
          <h3>${this.copyright}</h3>
          <p>
            <a href="https://lit.dev">Running on Lit</a> |
            <a href="https://vitejs.dev">Built by Vite</a>
          </p>
        </center>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "footer-element": FooterElement;
  }
}
