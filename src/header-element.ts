import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";

@customElement("header-element")
export class HeaderElement extends LitElement {
  @property()
  title = "Dive Planning Slate";

  @property()
  subTitle =
    '"75% of the Earth is water. Divers live on a much bigger planet."';

  render() {
    return html`
      <div>
        <center>
          <h1>${this.title}</h1>
          <h2>${this.subTitle}</h2>
        </center>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "header-element": HeaderElement;
  }
}
