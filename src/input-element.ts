import { LitElement, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import { IDiveStepBase } from "./table-element";
import { depthList, durationList, gasList } from "./config/constants";

@customElement("input-element")
export class InputElement extends LitElement {
  @property()
  selection: IDiveStepBase = {
    depth: 1,
    duration: 1,
    gas: "N21",
  };

  private _getDepthSelection() {
    this.selection.depth = Number(
      this.shadowRoot!.querySelector<HTMLSelectElement>("#select_depth")!.value
    );
  }

  private _getDurationSelection() {
    this.selection.duration = Number(
      this.shadowRoot!.querySelector<HTMLSelectElement>("#select_duration")!
        .value
    );
  }

  private _getGasSelection() {
    this.selection.gas =
      this.shadowRoot!.querySelector<HTMLSelectElement>("#select_gas")!.value;
  }

  private _saveDiveStep() {
    const event = new CustomEvent("save-dive-step", {
      detail: this.selection,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(event);
  }

  render() {
    return html`
      <div>
        <center>
          Please enter information:
          <div>
            Depth:
            <select id="select_depth" @input="${this._getDepthSelection}">
              <label>Select Depth</label>
              ${depthList.map(
                (depth) => html` <option value=${depth}>${depth}</option>`
              )}
            </select>
            Duration:
            <select id="select_duration" @input=${this._getDurationSelection}>
              <label>Select Duration</label>
              ${durationList.map(
                (duration) => html` <option value=${duration}>
                  ${duration}
                </option>`
              )}
            </select>
            Gas:
            <select id="select_gas" @input=${this._getGasSelection}>
              <label>Select Gas</label>
              ${gasList.map(
                (gas) => html` <option value=${gas}>${gas}</option>`
              )}
            </select>
            <button @click="${this._saveDiveStep}">Save</button>
          </div>
        </center>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "input-element": InputElement;
  }
}
