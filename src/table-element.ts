import { LitElement, html } from "lit";
import { customElement, property, state } from "lit/decorators.js";
import {
  calcRuntime,
  correctRuntime,
  calcFactor,
  calcVolume,
  calcPo2,
  calcCnsPerMin,
  calcCncPercent,
  calcOverview,
} from "./utils/calculator";
import {
  surfaceConstumptionRate,
  gas_map,
  tank_size,
  tank_pressure,
} from "./config/constants";
import { importCsvFileAsync } from "./utils/file-utils";

import "./input-element.ts";

export interface IDiveStepBase {
  depth: number;
  duration: number;
  gas: string;
}

export interface IDiveStep extends IDiveStepBase {
  id: number;
  runtime: number;
  sac: number;
  factor: number;
  volume: number;
  po2: number;
  cns_min: number | string;
  cns_percent: number | string;
}

export interface IOverview {
  volumeMap: Map<string, number>;
  totalCns: number;
}

interface IOverviewDetail {
  gas: string;
  req_volume: number;
  res_volume: number;
  total_volume: number;
  tank_size: number;
  tank_pressure: number;
  number_of_tanks: number;
}

@customElement("table-element")
export class TableElement extends LitElement {
  @property()
  diveSteps: IDiveStep[] = [];

  @property({ type: Number })
  diveStepCounter = 0;

  @property({ type: Number })
  runtime = 0;

  @property({ type: Number })
  cns_total = 0;

  @property()
  overview: IOverview = calcOverview(this.diveSteps);

  @property()
  gasOverviewDetail: IOverviewDetail[] = [];

  @state()
  private _inputElementHidden = true;

  _addDiveStep(diveStep: IDiveStepBase) {
    this._inputElementHidden = true;
    this.diveStepCounter++;
    this.runtime = calcRuntime(this.diveSteps, this.runtime);

    const factor = calcFactor(diveStep.depth);
    const volume = calcVolume(factor, diveStep.duration);
    const po2 = calcPo2(factor, gas_map.get(diveStep.gas)!);
    const cns_min = calcCnsPerMin(po2);
    const cns_percent = calcCncPercent(diveStep.duration, cns_min);

    this.diveSteps.push({
      id: this.diveStepCounter,
      depth: diveStep.depth,
      duration: diveStep.duration,
      runtime: this.runtime,
      sac: surfaceConstumptionRate,
      factor,
      volume,
      gas: diveStep.gas,
      po2,
      cns_min,
      cns_percent,
    });
  }

  _setGasOverviewDetail(overview: IOverview) {
    this.cns_total = Math.round(overview.totalCns * 100) / 100;
    this.gasOverviewDetail = [];
    for (const gas of Array.from(overview.volumeMap.keys())) {
      const req_volume = overview.volumeMap.get(gas)!;
      const res_volume = req_volume / 2;
      const total_volume = req_volume + res_volume;
      const number_of_tanks = Math.ceil(
        total_volume / (tank_size * tank_pressure)
      );
      this.gasOverviewDetail.push({
        gas,
        req_volume,
        res_volume,
        total_volume,
        tank_size,
        tank_pressure,
        number_of_tanks,
      });
    }
  }

  private _deleteDiveStep(e: number) {
    const tempArr: IDiveStep[] = this.diveSteps.filter(
      (diveStep: IDiveStep) => {
        return diveStep.id !== e;
      }
    );

    this.diveSteps = tempArr;
    correctRuntime(this.diveSteps);
  }

  _calculate() {
    this.overview = calcOverview(this.diveSteps);
    this._setGasOverviewDetail(this.overview);
  }

  _parseContent(content: string) {
    const diveSteps = content.split(/[\n]+/);
    diveSteps.forEach((diveStep) => {
      const temp = diveStep.split(/,/);
      if (temp.length === 3) {
        const diveStepBase: IDiveStepBase = {
          depth: parseInt(temp[0]),
          duration: parseInt(temp[1]),
          gas: temp[2],
        };
        this._addDiveStep(diveStepBase);
      }
    });
  }

  private _import() {
    importCsvFileAsync(
      this.shadowRoot!.querySelector<HTMLInputElement>("#csvImport"),
      this._parseContent.bind(this)
    );
  }

  render() {
    return html`
      <div>
        <table>
          <tr>
            <th>Depth</th>
            <th>Time</th>
            <th>Runtime</th>
            <th>SAC</th>
            <th>Conv Factor (ata)</th>
            <th>Volume</th>
            <th>Gas</th>
            <th>PO2</th>
            <th>%CNS/Min</th>
            <th>%CNS</th>
          </tr>
          ${this.diveSteps.map(
            (diveStep) => html` <tr>
              <th>${diveStep.depth}</th>
              <th>${diveStep.duration}</th>
              <th>${diveStep.runtime}</th>
              <th>${diveStep.sac}</th>
              <th>${diveStep.factor}</th>
              <th>${diveStep.volume}</th>
              <th>${diveStep.gas}</th>
              <th>${diveStep.po2}</th>
              <th>${diveStep.cns_min}</th>
              <th>${diveStep.cns_percent}</th>
              <th>
                <button @click="${() => this._deleteDiveStep(diveStep.id)}">
                  X
                </button>
              </th>
            </tr>`
          )}
        </table>
      </div>
      <div>
        <input-element
          @save-dive-step="${(event: CustomEvent) =>
            this._addDiveStep(event.detail)}"
          ?hidden=${this._inputElementHidden}
        >
        </input-element>
      </div>
      <div>
        <button @click="${() => (this._inputElementHidden = false)}">
          Add DiveStep
        </button>
        <button @click="${() => this._calculate()}">Calculate</button>
        <input type="file" id="csvImport" name="import" accept=".csv" />
        <button @click="${() => this._import()}">Import</button>
        <button @click="">Export</button>
      </div>
      <div>
        <center>
          <table>
            <tr>
              <th>Gas</th>
              <th>Required Volume</th>
              <th>Reserve</th>
              <th>Total</th>
              <th>Tank Size</th>
              <th>Tank Pressure</th>
              <th>Number of Tanks</th>
            </tr>
            ${this.gasOverviewDetail.map(
              (individualGas) => html` <tr>
                <th>${individualGas.gas}</th>
                <th>${individualGas.req_volume} L</th>
                <th>${individualGas.res_volume} L</th>
                <th>${individualGas.total_volume} L</th>
                <th>${individualGas.tank_size} L</th>
                <th>${individualGas.tank_pressure} Bar</th>
                <th>${individualGas.number_of_tanks}</th>
              </tr>`
            )}
          </table>
          <p>Total CNS: ${this.cns_total}</p>
        </center>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    "table-element": TableElement;
  }
}
