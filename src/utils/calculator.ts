import {
  surfaceConstumptionRate,
  cns_min_map,
  notAvailable,
} from "../config/constants";
import { IDiveStep, IOverview } from "../table-element";

export const calcRuntime = (
  diveSteps: IDiveStep[],
  currentRuntime: number
): number => {
  if (diveSteps.length === 0) {
    return 0;
  }
  return currentRuntime + diveSteps[diveSteps.length - 1].duration;
};

export const correctRuntime = (diveSteps: IDiveStep[]) => {
  let runtime = 0;
  diveSteps.forEach((diveStep) => {
    diveStep.runtime = runtime;
    runtime += diveStep.duration;
  });
};

export const calcFactor = (depth: number): number => {
  return 1 + depth / 10;
};

export const calcVolume = (factor: number, duration: number): number => {
  return factor * surfaceConstumptionRate * duration;
};

export const calcPo2 = (factor: number, gas: number): number =>
  Math.round(factor * gas * 1000) / 1000;

export const calcCnsPerMin = (po2: number): number | string => {
  const cnsPerMin = cns_min_map.get(Math.round(po2 * 10) / 10);
  return cnsPerMin ?? notAvailable;
};

export const calcCncPercent = (
  duration: number,
  cnsPerMin: number | string
): number | string => {
  return typeof cnsPerMin === "string"
    ? notAvailable
    : duration * (Math.round(cnsPerMin * 100) / 100);
};

export const calcOverview = (diveSteps: IDiveStep[]): IOverview => {
  const volumeMap = new Map<string, number>();
  let totalCns = 0;
  diveSteps.forEach((diveStep) => {
    if (diveStep.cns_percent !== notAvailable) {
      totalCns += Number(diveStep.cns_percent);
    }

    if (volumeMap.has(diveStep.gas)) {
      volumeMap.set(
        diveStep.gas,
        volumeMap.get(diveStep.gas)! + diveStep.volume
      );
    } else {
      volumeMap.set(diveStep.gas, diveStep.volume);
    }
  });

  return {
    volumeMap,
    totalCns,
  };
};
