type ParseContent = (c: string) => void;

export const importCsvFileAsync = (
  csvImport: HTMLInputElement | null,
  parseContent: ParseContent
) => {
  if (csvImport !== null && csvImport.files && csvImport.files.length > 0) {
    try {
      const csvReader = new FileReader();
      csvReader.readAsText(csvImport.files[0]);
      csvReader.onload = (event) => {
        if (event.target !== null) {
          parseContent(event.target.result as string);
        }
        return "";
      };
    } catch (e) {
      if (e instanceof Error) {
        console.log(e.message);
      }
    }
  }
  return "";
};
